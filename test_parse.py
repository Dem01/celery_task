import json
import requests

import pytest
import httpretty

from parse import get_link, process, is_url

class TestParsing:
    def setup(self):
        with open('response.json', 'r') as resp:
            self.response = json.load(resp)
        with open('trojmiasto.htm', 'r') as p:
            self.page = p.read()

    @httpretty.activate
    def test_get_link(self):
        url = 'https://httpstat.us/200'
        httpretty.register_uri(httpretty.GET,
                               url,
                               body=self.page)
        assert get_link(url) == self.response['get_link']['urls']    
    
    @httpretty.activate
    def test_no_links_get_link(self):
        url = 'https://httpstat.us/200'
        httpretty.register_uri(httpretty.GET,
                               url,
                               body='abc')
        assert get_link(url) == []
    
    @httpretty.activate
    def test_is_url(self):
        url = 'https://httpstat.us/200'
        httpretty.register_uri(httpretty.GET,
                               url,
                               status=200)
        assert is_url(url) == url

    def test_bad_url(self, capsys):
        with pytest.raises(SystemExit):
            is_url('htp:/badurl')
        out, err = capsys.readouterr()
        assert out == "Bad URL\n"

    @httpretty.activate
    def test_bad_response(self):
        url = 'https://httpstat.us/200'
        httpretty.register_uri(httpretty.GET,
                               url,
                               status=404)
        assert get_link(url) == "no response"

        

class TestProduct:
    def setup(self):
        with open('response.json', 'r') as resp:
            self.response = json.load(resp)
    
    @httpretty.activate
    def test_process(self):
        url = 'https://httpstat.us/200'
        httpretty.register_uri(httpretty.GET,
                               url,
                               body='<h1 class="title">Tusze i tonery, zamienniki i oryginalne, regeneracja</h1><a data-fancybox="photo" href="https://s-trojmiasto.pl/ogloszenia/foto/60430000/60428885/9_Tusze-i-tonery-zamienniki-i-oryginalne-regeneracja_900x700.jpg"> <span class="oglDetailsMoney">40<span class="oglDetailsMoney__label"> zł</span></span>',
                               status=200)
        assert process(url)['title'] == self.response['test_process_all_good']['title']
        assert process(url)['photo'].replace(" ", "") == self.response['test_process_all_good']['photo']
        assert process(url)['price'] == self.response['test_process_all_good']['price']
    
    @httpretty.activate
    def test_process_no_photo(self):
        url = 'https://httpstat.us/200'
        httpretty.register_uri(httpretty.GET,
                               url,
                               body='<h1 class="title">Tusze i tonery, zamienniki i oryginalne, regeneracja</h1><span class="oglDetailsMoney">40<span class="oglDetailsMoney__label"> zł</span></span>',
                               status=200)
        assert process(url)['title'] == self.response['test_process_no_photo']['title']
        assert process(url)['photo'].replace(" ", "") == self.response['test_process_no_photo']['photo']
        assert process(url)['price'] == self.response['test_process_no_photo']['price']

