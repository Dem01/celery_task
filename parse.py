import sys
import logging
import requests

from bs4 import BeautifulSoup
from celery import Celery, task, group, chain, chord

logging.basicConfig(level=logging.DEBUG, format='%(asctime)s - %(levelname)s - %(message)s',
                    datefmt="%d-%m-%Y %H:%M:%S")

app = Celery('parse',
            broker='redis://localhost:6379/0',
            backend='redis://localhost:6379/1')

@app.task
def get_link(url):
    
    res = requests.get(f'{url}')
    if res.status_code == 200:
        con = res.content
        soup = BeautifulSoup(con, features='html.parser')
        links = []
        data = soup.find_all('a', 'list__item__content__title__name link')
        for i in data:
            links.append(i.get('href'))
        return links
    else:
        return 'no response'

@app.task
def process(url):
    res = requests.get(f'{url}')
    if res.status_code == 200:
        content = res.content
        soup = BeautifulSoup(content, features='html.parser')
        title = soup.find('h1').string.strip()
        try:
            photo = soup.find('a', {'data-fancybox' : "photo"}).get('href')
        except AttributeError:
            photo = "no photo provided"
        price = soup.find('span', {'class' : 'oglDetailsMoney'})
        price_ = ''.join(i for i in str(price) if i.isdigit()) 
        return {'title' : title,
                'photo' : photo,
                'price' : price_}
    else:
        return 'no response'

@app.task
def feed_map(urls):
    return group(process.s(url) for url in urls)()

def is_url(url):
  try:
    res = requests.get(url)
    return url
  except:
    print("Bad URL")
    raise SystemExit(1)
    

if __name__ == '__main__':
    if len(sys.argv) != 3:
        msg = "Bad usage - try: 'python parse.py --url --page_num'"
        logging.error(msg)
        exit(1)
    try:
            url, page = sys.argv[1], sys.argv[2]
            
            if not is_url(url):
                logging.error("Bad URL")
                exit(1)
            pages = [f"{url}?strona={i}" for i in range(int(page))]
            group(chain(get_link.s(page), feed_map.s()) for page in pages)()
            
            #print details
            for url in pages:
                products_links = get_link(url)
                if get_link(url) == 'no response':
                    continue
                logging.info(f"Wyniki z {url}:\n")
                for link in products_links:
                    details = process(link)
                    if details == 'no response':
                        continue
                    msg = f"{details['title']} {details['photo']} {details['price']}" 
                    logging.info(msg)
    except:
        msg = f"Unexpected error: {sys.exc_info()[0]}"
        logging.error(msg)
        raise
