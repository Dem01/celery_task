# Trojmiasto.pl - adverts parser

A little script to fetch some data from ogloszenia.trojmiasto.pl

## Installation

Use the package manager [pip](https://pip.pypa.io/en/stable/) to install.
- Clone this repo, setup your VENV and:
```bash
pip install -r requirements.txt
```

## Usage
- run broker(e.g. redis)
- run celery
```python
celery worker -A parse -l info
```
- script
```python
python parse.py --url --page_number
```

## Running the tests

```python
py.test -v test_parse.py
```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.
Please make sure to update tests as appropriate.

## License
This project is licensed under the MIT License - see the [LICENSE.md](https://gitlab.com/Dem01/celery_task/blob/master/LICENSE) file for details